//
//  main.m
//  WarhammerRPIphone
//
//  Created by Daniel Teige on 2013-10-17.
//  Copyright (c) 2013 Daniel Teige. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DTAppDelegate class]));
    }
}
