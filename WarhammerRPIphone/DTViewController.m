//
//  DTViewController.m
//  WarhammerRPIphone
//
//  Created by Daniel Teige on 2013-10-17.
//  Copyright (c) 2013 Daniel Teige. All rights reserved.
//

#import "DTViewController.h"

@interface DTViewController ()

@end

@implementation DTViewController
@synthesize WSTextField;
@synthesize StrengthTextField;
@synthesize AttacksTextField;

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    DTMeleeViewController *destination = [segue destinationViewController];
    
    //TABLE VIEW TESTET
   /* DTMeleeTableViewController *OTHERdestination = [segue destinationViewController];*/
    //Texten i texfieldsen skickas till NSStringarna i DTMeleeViewController
   /*destination.wsText= [WSTextField text];
    destination.sText = [StrengthTextField text];
   destination.aText = [AttacksTextField text];*/
    
    //Test med array istället för att sätta text fälten
    NSMutableArray *MainstatsArray = [[NSMutableArray alloc] init];
    
    [MainstatsArray addObject:[WSTextField text]];
    [MainstatsArray addObject:[StrengthTextField text]];
    [MainstatsArray addObject:[AttacksTextField text]];
    
    NSLog(@"vad finns på plats 0 %@",MainstatsArray[0]);
    destination.statsArray =MainstatsArray;
  //  OTHERdestination.OTHERstatsArray = MainstatsArray;
 
}

-(IBAction)returned:(UIStoryboardSegue *)segue {
   // _AttacksTextField.text = @"3";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
