//
//  DTAppDelegate.h
//  WarhammerRPIphone
//
//  Created by Daniel Teige on 2013-10-17.
//  Copyright (c) 2013 Daniel Teige. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
