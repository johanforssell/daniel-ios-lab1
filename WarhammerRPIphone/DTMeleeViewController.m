//
//  DTMeleeViewController.m
//  WarhammerRPIphone
//
//  Created by Daniel Teige on 2013-10-17.
//  Copyright (c) 2013 Daniel Teige. All rights reserved.
//

#import "DTMeleeViewController.h"

@interface DTMeleeViewController ()

@end

@implementation DTMeleeViewController


- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if (self) {
    }
    return self;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   // NSLog(@"what is count? %lu", (unsigned long)[combatLogArray count]);
    return [self.combatLogArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"I_have_to_set_this";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
   
    
    cell.textLabel.text = [self.combatLogArray objectAtIndex:indexPath.row];
    NSLog(@"U EVEN RUN?");
    // Configure the cell...
    /*int rowNumber = [indexPath row];
    NSString *stats = [combatLogArray objectAtIndex:rowNumber];
    
    [[cell textLabel] setText:stats];*/
    return cell;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    NSString *a = [self composeRollMessageForHit:YES roll:10 damage:15];
    NSString *b = [self composeRollMessageForHit:NO roll:80 damage:0];
    self.combatLogArray = [NSMutableArray arrayWithArray:@[a, b]];
    
    NSLog(@"VIEWDIDLOAD");
  
    self.WSLabel.text = self.statsArray[0];
    self.SLabel.text = self.statsArray[1];
    self.ALabel.text = self.statsArray[2];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)CombatRoll:(int)wsSkill{
    
    
    int diceRolled= (arc4random() % 100)+1;
    
    if(wsSkill >= diceRolled){
        
        return TRUE;
    }
    else{
        return FALSE;
    }
}

- (IBAction)OneAttackButton:(id)sender {
    DTMeleeViewController *melee = [[DTMeleeViewController alloc] init];
    self.diceroll = [self.statsArray[0] integerValue];
    
    BOOL hit=[melee CombatRoll:self.diceroll];
    if(hit){
        [self.CombatTextField setText:@"HIT"];
    }else{
        [self.CombatTextField setText:@"MISS"];
    }
    
    [self.tableView reloadData];
}

- (IBAction)FullAttackButton:(id)sender {
    //TODO THIS BUTTON
}

- (NSString *)composeRollMessageForHit:(BOOL)hit roll:(NSInteger)roll damage:(NSInteger)damage {
    return [NSString stringWithFormat:@"%@ rolled %d; Damage %d", (hit?@"Hit":@"Miss"), roll, damage];
}

@end
