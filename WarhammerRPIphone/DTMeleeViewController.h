//
//  DTMeleeViewController.h
//  WarhammerRPIphone
//
//  Created by Daniel Teige on 2013-10-17.
//  Copyright (c) 2013 Daniel Teige. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DTMeleeViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong,nonatomic) NSString *wsText;
@property (strong,nonatomic) NSString *sText;
@property (strong,nonatomic) NSString *aText;
@property (readwrite)int diceroll;
@property (strong, nonatomic) IBOutlet UILabel *WSLabel;
@property (strong, nonatomic) IBOutlet UILabel *SLabel;
@property (strong, nonatomic) IBOutlet UILabel *ALabel;
@property (strong, nonatomic) NSMutableArray *statsArray;
@property (strong, nonatomic) NSMutableArray *combatLogArray;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UITextField *CombatTextField;

- (IBAction)OneAttackButton:(id)sender;
- (IBAction)FullAttackButton:(id)sender;
- (BOOL)CombatRoll:(int)wsSkill;

@end
