//
//  DTViewController.h
//  WarhammerRPIphone
//
//  Created by Daniel Teige on 2013-10-17.
//  Copyright (c) 2013 Daniel Teige. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTMeleeViewController.h"

@interface DTViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *WSTextField;
@property (weak, nonatomic) IBOutlet UITextField *StrengthTextField;
@property (weak, nonatomic) IBOutlet UITextField *AttacksTextField;

@end
